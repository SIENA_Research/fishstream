# README #

Within the framework of the Fishstream project, a popular open source particle image velocimetry tool [PIVlab](https://pivlab.blogspot.com/) was adapted to to account for the specific project needs. These included image stabilisation, loading reference measurements into the PIVlab GUI and identification of calculated values in any given point of the resulting vector field, not only at vector locations. The version of PIVlab used at the time of the development of the extensions (2017-2019) was 2.02 and we do not plan to update it. The use of stabilisation algorithm does not require a GUI.

### What is this repository for? ###

* image stabilisation for velocimetry purposes
* comparison with reference values in PIVlab 2.02
* creation of raster velocity fields in PIVlab 2.02 overlaid with motionless areas of the study area (e.g. river banks)


### How do I get set up? ###

* An example of image stabilisation is given in code/testStab.m. You need at least 4 point pairs that are associated with GCPs or alike features. Method ```shiftCalc``` estimates displacement of the features. Method ```transform``` performs image transformation.
* You can run an extended version of PIVlab 2.02 by starting ```PIVlab_GUI.m```


### Acknoledgements ###

* PIVlab was created and is maintained by William Thielicke. Its [repository](https://github.com/Shrediquette/PIVlab) can be found on GitHub.

### Who do I talk to? ###

* Leave a comment or contact Evnica at evnica[at]gmail.com