% read file from the disk
withVelocity = 0;
% let user select an ASCII file to read
[inputAsciiName, inputAsciiPath] = uigetfile('*.txt', 'Select an ASCII file');

% read the file into an array
if withVelocity == 0
    formatSpec = '%d %d %f %f';
    colNum = 4;
else
    formatSpec = '%d %d %f %f %f';
    colNum = 5;
end

asciiFile = fopen(fullfile(inputAsciiPath, inputAsciiName), 'r');
sizeOfArray = [colNum inf];

vectorData = fscanf(asciiFile, formatSpec, sizeOfArray);
vectorData = vectorData.';
fclose(asciiFile);

disp('Done')