function extract(path_to_video, output_folder, start_frame, end_frame, step)

disp('Reading the video file...')
vid = VideoReader(path_to_video);

if isnan(end_frame)
    duration = vid.Duration;
    fps = vid.FrameRate;
    end_frame = duration * fps;
end

if isnan(start_frame)
    start_frame = 0;
end

for i = start_frame:step:end_frame
    filename = strcat(output_folder, sprintf('%05d', i), '.jpg');
    img = read(vid, i);
    imwrite(img, filename);
    disp([num2str(i) '/' num2str(end_frame) ' extracted'])
end


function extractWithVReader(vid, output_folder, start_frame, end_frame, step)

disp('Reading the video file...')

for i = start_frame:step:end_frame
    filename = strcat(output_folder, sprintf('%05d', i), '.jpg');
    img = read(vid, i);
    imwrite(img, filename);
    disp([num2str(i) '/' num2str(end_frame) ' extracted'])
end