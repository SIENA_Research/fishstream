distanceFromGCP = 96;
path = 'E:\projects\FishStream_Data\20190807_Rottau\cmpare\';

img1 = imread(strcat(path, 'rudi.jpg'));
img2 = imread(strcat(path, 'ulf.jpg'));

gcpsRudi = [666 171; 1404 180; 1923 219; 2367 294; ...
            3375 279; 1170 1035; 1764 996; 3549 849; ...
            2760 1359; 1644 1755; 3345 1911];
gcpsUlf = [69 207; 867 198; 1404 219; 1863 264;...
           2895 150; 654 1092; 1272 1014; 3147 753;...
           2340 1338; 1176 1827; 3015 1872];
       
% transform(img1, img2, strcat(path, 'ulfTrans.jpg'), gcpsRudi, gcpsUlf, 2);
transform(img2, img1, strcat(path, 'rudiTrans.jpg'), gcpsUlf, gcpsRudi, 2);