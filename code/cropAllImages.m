% crops all images in the input directory inputDir to dimensions specified
% by xmin, ymin, width, height; results are stored into outputDir
% beware: not idiot-proof

function cropAllImages(inputDir, outputDir, xmin, ymin, width, height)
disp('enter cromAllImages()');
imgs = imageDatastore(inputDir, 'FileExtensions',{'.jpg','.png', '.tiff'});
for i = 1:size(imgs.Files, 1)
    imgName = imgs.Files{i};
    [~, name, ext] = fileparts(imgName);
    disp(['processing ' name]);
    outputPath = strcat(outputDir, '\', name, ext);
    img = imread(imgName);
    cropped = imcrop(img, [xmin, ymin, width, height]);
    imwrite(cropped, outputPath);
end
disp('exit cromAllImages()');
