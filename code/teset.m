
path_to_video = 'E:\projects\FishStream_Data\20190807_Rottau\rudi\allTracer_DJI_0001\';
video = strcat(path_to_video, 'DJI_0001.MOV');
vid = VideoReader(path_to_video);
videoOutputFolder = strcat...
                (uigetdir(path_to_video, 'Select a folder for frame output'), '\');
fps = vid.FrameRate;
disp([num2str(fps) ' fps']);
startSecond = 2 * 60 + 40; % 2:40
endSecond = 4 * 60 + 34; % 4:34
startFrame = fps * startSecond;
endFrame = fps * endSecond;
step = 2;
disp('Extracting frames');
disp(['Start frame: ' num2str(startFrame)]);
disp(['End frame: ' num2str(endFrame)]);
extractWithVReader(vid, videoOutputFolder, startFrame, endFrame, step);