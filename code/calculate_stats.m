% calculates statistics based on the results list of PIV analysis performed
% by PIVlab

function stats_frame = calculate_stats(results_list, vel_scale, u_i, v_i, last_pair)

% load results list from the workspace or from an external .mat file
if(isnan(results_list))
    results_list = load(resultslist);
else
    results_list = load(results_list);
    results_list = results_list.resultslist;
end

% if select frames for statistics calculation (discard some frames at the
% end if necessary)
if (isnan(last_pair))
    num_of_pairs = size(results_list, 2);
else
    num_of_pairs = last_pair;
end

field_size = size(results_list{1, 1});
velocities = zeros(field_size(1), field_size(2), num_of_pairs);

% calculate velocities based on vectors u and v; u_i and v_i ar indeces of
% vectors to be used (e.g. 7 and 8 in PIVlab stand for filtered vectors)
for i = 1:num_of_pairs
    u = results_list{u_i, i};
    v = results_list{v_i, i};
    u2 = u.^2;
    v2 = v.^2;
    sum_of_squares = u2 + v2;
    ith_velocities = sum_of_squares.^0.5;
    velocities(:, :, i) = ith_velocities.*vel_scale;
end

% calculte statistics: mean, std. deviation, max, min and median
mean_vel = nanmean(velocities, 3);
median_vel = nanmedian(velocities, 3);
std_vel = std(velocities, 0, 3,'omitnan');
max_vel = max(velocities, [], 3);
min_vel = min(velocities, [], 3);
% get coordinates of vectors
xs = results_list{1, 1};
ys = results_list{2, 1};

% store calculated stats into the stats_frame object
stats_frame.xs = xs;
stats_frame.ys = ys;
stats_frame.mean_vel = mean_vel;
stats_frame.std_vel = std_vel;
stats_frame.median_vel = median_vel; 
stats_frame.max_vel = max_vel;
stats_frame.min_vel = min_vel;

disp('done');
