% input images to stabilize
path_to_video = 'E:\projects\FishStream_Data\Matlab_tests\mosaicTest\';
video = strcat(path_to_video, 'DJI_0179.mp4');
videoOutputFolder = strcat...
                (uigetdir(path_to_video, 'Select a folder for frame output'), '\');
fps = 25;
startSecond = 10;
endSecond = 120;
startFrame = fps * startSecond;
endFrame = fps * endSecond;
step = 2;
disp('Extracting frames');
extract(video, videoOutputFolder, startFrame, endFrame, step);

inputDir = videoOutputFolder;
outputDir = uigetdir(inputDir, 'Select a folder for stabilized output');

distanceFromGCP = 64;

imgs = imageDatastore(inputDir, 'FileExtensions',{'.jpg','.png'});
modelImage = imgs.Files{1};
img1 = imread(modelImage);

% define gcps
gcps = [3137 1336; 3366 790; 3421 1391];

for i = 2:size(imgs.Files, 1)
    img2Name = imgs.Files{i};
    [~, name, ext] = fileparts(img2Name);
    outputPath = strcat(outputDir, '\', name, ext);
    
    % read images 
    img2 = imread(img2Name);

    % calculate shift in gcps
    newGcps = shiftCalc(img1, img2, gcps, distanceFromGCP);

% transform (affine) the second image and store it in the output directory

transform(img1, img2, outputPath, gcps, newGcps);
disp(strcat(name, ext, ' transformed'));
end    

%  write the model image to the output directory
[~, name, ext] = fileparts(modelImage);
outputPath = strcat(outputDir, '\', name, ext);
imwrite(img1, outputPath);










% % read images 
% img1 = imread(strcat(dir, img1Name));
% img2 = imread(strcat(dir, img2Name));
% 
% % define gcps
% gcps = [3133 1332; 3434 1195; 3417 1388];
% 
% % calculate shift in gcps
% newGcps = shiftCalc(img1, img2, gcps);
% 
% % transform (affine) the second image
% transform(inputDir, outputDir, img1Name, img2Name, gcps, newGcps);
% disp('Done');



% passes = 2; % is enough for to calculate GCPs displacement
% 
% % input images to stabilize
% dir = 'E:\projects\FishStream_Data\Matlab_tests\extract_test\test_pystab\'; 
% img1 = imread(strcat(dir, '13.jpg'));
% img2 = imread(strcat(dir, '14.jpg'));
% 
% % settings of fft, probably can stay unchanged for most cases
% distanceFromGCP = 128;
% interrogationarea = distanceFromGCP;
% step = distanceFromGCP / 2;
% roiWidth = distanceFromGCP * 2;
% roiHeight = distanceFromGCP * 2;
% subpixfinder = 1;
% mask = [ ];
% int2 = distanceFromGCP / 2;
% int3 = int2 / 2;
% int4 = int3 / 2;
% imdeform = '*spline';
% if passes > 1
%     repeat = 1;
% else
%     repeat = 0;
% end
% mask_auto = 0;
% 
% % no hard-coding possible here: coords reference individual frames
% % user should be able to select them from GUI
% gcps = [3133 1332; 3434 1195; 3417 1388];
% newGcps = zeros(size(gcps));
% 
% for i = 1: size(gcps, 1)
%     gcpX = gcps(i, 1);
%     gcpY = gcps(i, 2);
%     roiTopLeftX = gcpX - distanceFromGCP;
%     roiTopLeftY = gcpY - distanceFromGCP;
%     roirect = [roiTopLeftX roiTopLeftY roiWidth roiHeight];
% 
% 
%     [x, y, u, v, typevector] = piv_FFTmulti (img1, img2, ...
%                                              interrogationarea, step, ...
%                                              subpixfinder, mask, roirect, ...
%                                              passes, int2, int3, int4, ...
%                                              imdeform, repeat, mask_auto);
% 
%     disp('Vectors calculated');
%     gridSize = size(x, 1);
% 
%     if mod(gridSize, 2) > 0
%         middle = ceil(gridSize/2);
%         neighborU = u(middle-1:middle+1, middle-1:middle+1);
%         neighborV = v(middle-1:middle+1, middle-1:middle+1);
%     else
%         middle = gridSize / 2;
%         neighborU = u(middle:middle+1, middle:middle+1);
%         neighborV = v(middle:middle+1, middle:middle+1);
%     end
% 
%     uShift = mean(mean(neighborU));
%     vShift = mean(mean(neighborV));
%     
%     uShift = round(uShift, 4);
%     vShift = round(vShift, 4);
% 
%     newX = gcpX + uShift;
%     newY = gcpY + vShift;
%     
%     newGcps(i, 1) = newX;
%     newGcps(i, 2) = newY;
% 
% end
% 
% disp('Done');