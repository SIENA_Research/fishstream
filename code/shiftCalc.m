% calculate the shift of img2 relative to img1 based on gcps (3 coordinate
% pairs) in img1
% distanceFromGCPs determines the size of the area around the GCPs
% (4 x distanceFromGCP) within which the shift is calculated
% extra points are necessary in case vectors can't be calculated for GCPs
% and ideally forming a fat triangle
% extra points should be not shorter than GCPs

function transformationPoints = ...
            shiftCalc(img1, img2, gcps, extraPoints, distanceFromGCP)

newGcps = zeros(size(gcps));
nansPresent = 0;

for i = 1: size(gcps, 1)
    gcpX = gcps(i, 1);
    gcpY = gcps(i, 2);
    
    oldX = NaN;
    oldY = NaN;
    newX = NaN;
    newY = NaN;
    j = i;
    
    while isnan(oldX) && j <= size(extraPoints, 1)
        
        [oldX, oldY, newX, newY] = calcVectors(img1, img2, gcpX, gcpY, distanceFromGCP);
        % if displacement for the current point could not be calculated
        if isnan(oldX) && j < size(extraPoints, 1)
            % replace a gcp with an extra point
            gcps(i, 1) = extraPoints(j, 1);
            gcps(i, 2) = extraPoints(j, 2);
            j = j + 1;
        end
    end   
    
    if isnan(oldX)
        nansPresent = 1;
    else
        disp([num2str(oldX) '    ' num2str(oldY)]);
        disp([num2str(newX) '    ' num2str(newY)]);
        gcps(i, 1) = oldX;
        gcps(i, 2) = oldY;
        newGcps(i, 1) = newX;
        newGcps(i, 2) = newY;
    end
end

if nansPresent
    transformationPoints = NaN;
else
    transformationPoints = [gcps, newGcps];
end




function [oldX, oldY, newX, newY] = calcVectors(img1, img2, gcpX, gcpY, distanceFromGCP)
    passes = 1; % is enough for to calculate GCPs displacement
    % settings of fft, probably can stay unchanged for most cases
    interrogationarea = distanceFromGCP; % or 64px if gcps are near water and the shift is small 
    step = interrogationarea / 2;   
    roiWidth = distanceFromGCP * 1;
    roiHeight = distanceFromGCP * 1;
    subpixfinder = 1;
    mask = [ ];
    int2 = interrogationarea / 2;
    int3 = int2 / 2;
    int4 = int3 / 2;
    imdeform = '*linear'; % can be a parameter, but since there are only 3 points, performance should still be ok
    if passes > 1
        repeat = 1;
    else
        repeat = 0;
    end
    mask_auto = 0;

    roiTopLeftX = gcpX - distanceFromGCP;
    if roiTopLeftX < 0
        roiTopLeftX = 4;
    end
    roiTopLeftY = gcpY - distanceFromGCP;
    if roiTopLeftY < 0
        roiTopLeftY = 4;
    end
    roirect = [roiTopLeftX roiTopLeftY roiWidth roiHeight];


    [x, y, u, v, ~] = piv_FFTmulti (img1, img2, ...
                                             interrogationarea, step, ...
                                             subpixfinder, mask, roirect, ...
                                             passes, int2, int3, int4, ...
                                             imdeform, repeat, mask_auto);

    %disp('Vectors calculated');
    gridSize = size(x, 1);
    
    if (isnan(x) || isnan(y) || isnan(u) || isnan(v))
        oldX = NaN;
        oldY = NaN;
        newX = NaN;
        newY = NaN;
    else
        if gridSize == 1   
            oldX = x;
            oldY = y;
            newX = x+u;
            newY = y+v;
        else
            if mod(gridSize, 2) > 0
                middle = ceil(gridSize/2);
            else
                middle = gridSize / 2;
            end
                oldX = x(middle-1:middle+1, middle-1:middle+1);
                oldY = y(middle-1:middle+1, middle-1:middle+1);
                newX = x + u(middle-1:middle+1, middle-1:middle+1);
                newY = y + v(middle-1:middle+1, middle-1:middle+1);
        end
    end
    
    function [neighborU, neighborV] = calcVectors2(img1, img2, gcpX, gcpY, distanceFromGCP)
    passes = 1; % is enough for to calculate GCPs displacement
    % settings of fft, probably can stay unchanged for most cases
    interrogationarea = distanceFromGCP; % or 64px if gcps are near water and the shift is small 
    step = interrogationarea / 2;   
    roiWidth = distanceFromGCP * 2;
    roiHeight = distanceFromGCP * 2;
    subpixfinder = 1;
    mask = [ ];
    int2 = interrogationarea / 2;
    int3 = int2 / 2;
    int4 = int3 / 2;
    imdeform = '*spline'; % can be a parameter, but since there are only 3 points, performance should still be ok
    if passes > 1
        repeat = 1;
    else
        repeat = 0;
    end
    mask_auto = 0;

    roiTopLeftX = gcpX - distanceFromGCP;
    roiTopLeftY = gcpY - distanceFromGCP;
    roirect = [roiTopLeftX roiTopLeftY roiWidth roiHeight];


    [x, ~, u, v, ~] = piv_FFTmulti (img1, img2, ...
                                             interrogationarea, step, ...
                                             subpixfinder, mask, roirect, ...
                                             passes, int2, int3, int4, ...
                                             imdeform, repeat, mask_auto);

    %disp('Vectors calculated');
    gridSize = size(x, 1);

    if mod(gridSize, 2) > 0
        middle = ceil(gridSize/2);
        neighborU = u(middle-1:middle+1, middle-1:middle+1);
        neighborV = v(middle-1:middle+1, middle-1:middle+1);
    else
        middle = gridSize / 2;
        neighborU = u(middle:middle+1, middle:middle+1);
        neighborV = v(middle:middle+1, middle:middle+1);
    end
    