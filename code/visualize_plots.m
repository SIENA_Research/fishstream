% create boxplot of std
stats = calculate_stats('G:\2020-05-20\Projects\Fishstream\Edling_Test_2020-05\analysis\27-05\m600\0527-TR2-2\2_5fps\0527-TR2-2_256_128_64_32_ftd_results.mat', 0.04975, 7, 8, 72);
% flatten the velocitie grid
nr = 66*119;
flat_std_vel = reshape(stats.std_vel, nr, 1);
flat_vel = reshape(stats.mean_vel, nr, 1);

%find indices of elements that satisfy the condition and select them from
%the corresponding arrays
ind_vel = find((0.25 < flat_vel) & (flat_vel <= 0.8));
% ind_vel = find(flat_vel <= 0.25);
std_vel = flat_std_vel(ind_vel);
vel = flat_vel(ind_vel);

%create a matrix for placing the plots near each other
group = [ ones(size(std_vel)); 2 * ones(size(vel))];
% group2 = [ ones(size(flat_std_vel)); 2 * ones(size(flat_vel))];

% create boxplots
f2=figure;
screensize=get( 0, 'ScreenSize' );
rect = [screensize(3)/4-300, screensize(4)/2-250, 600, 500];
set(f2, 'position', rect);
set(f2, 'numbertitle', 'off', 'toolbar', 'figure', ...
    'dockcontrols', 'off', 'name', 'Statistics ', 'tag', 'boxplot');
% h2=scatter(u*caluv-retr('subtr_u'),v*caluv-retr('subtr_v'), 'r.');

std_vel_plot = boxplot([std_vel, vel], group);
% std_vel_plot2 = boxplot([flat_std_vel, flat_vel], group2);
set (gca, 'xgrid', 'on', 'ygrid', 'on', 'TickDir', 'in')
set(gca,'XTickLabel',{'std','mean vel'})

disp('exit viz');