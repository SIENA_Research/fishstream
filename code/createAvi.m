function createAvi(path, videoName)
    if isnan(path)
        path = 'C:\Projects\';
    end
    if isnan(videoName)
        videoName = strcat(path, 'video.avi');
    else
        videoName = strcat(path, videoName);
    end
    inputFolder = uigetdir(path, 'Select a folder with images');
    fps = 25;
    step = 1;
    createAviVideo(videoName, inputFolder, fps, step);
end

function createAviVideo (videoName, inputFolder, fps, step)
    disp('in @createAviVideo');
    v = VideoWriter(videoName);
    v.Quality = 90;
    v.FrameRate = fps;
    open(v);
    imgs = imageDatastore(inputFolder, 'FileExtensions',{'.jpg','.png'});
        for i = 1:step:size(imgs.Files, 1)
            imgName = imgs.Files{i};
            img = imread(imgName);
            writeVideo(v, img);
        end
    close(v);
    disp('exit @createAviVideo');
end