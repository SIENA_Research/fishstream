function copy_nth(n, start_idx)

input_dir = uigetdir('Select an input folder');
output_dir = uigetdir(input_dir, 'Select an outout folder');

imgs = imageDatastore(input_dir, 'FileExtensions', {'.jpeg', '.jpg', '.png', '.tiff', '.tif'});
nr_of_imgs = size(imgs.Files, 1);
nr_to_copy = ceil((nr_of_imgs - start_idx) / n);
disp(['The input folder contains ' nr_of_imgs ' images']);
disp([num2str(nr_to_copy) ' images must be copied']);

for i = start_idx:n:nr_of_imgs
	img = imgs.Files{i};
	[~, name, ext] = fileparts(img);
	disp([num2str(nr_to_copy) ': copying ' name]);
	output_path = strcat(output_dir, '\', name, ext);
	img_array = imread(img);
	imwrite(img_array, output_path);
    nr_to_copy = nr_to_copy - 1;
end
disp('exit copy_nth');