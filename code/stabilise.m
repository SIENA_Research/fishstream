function stabilise (gcps, IA, step, nonModelNr, transType)

base_dir = 'C:\Projects\Fishstream\';
inputDir = uigetdir(base_dir, 'Select an input folder');
outputDir = uigetdir(base_dir, 'Select a folder for stabilized output');
[file,path] = uigetfile(strcat(base_dir, '\*.jpg'), 'Select a model image'); % or '\*.tif'
if isnan(transType)
    transType = 'affine'; % 'projective'; % 
end

imgs = imageDatastore(inputDir, 'FileExtensions',{'.jpg','.png'});
img1 = imread(strcat(path, file));
if isnan(gcps)
    % alpine: [400, 303; 2427, 320; 3709, 271; 3239, 1720; 1664, 1811; 532, 2001]
    % Basento: [103, 127; 126, 210; 113, 590; 65, 951; 177, 1033; 1529, 124; 1788, 257; 1745, 889; 1513, 778; 1421, 944];
    % edling 22 07 2020 [233, 431; 1085, 769; 1802, 117; 3546, 353; 2889, 438]
    gcps = [233, 431; 1085, 769; 1802, 117; 3546, 353; 2889, 438];
end

for i = nonModelNr:step:size(imgs.Files, 1)
    img2Name = imgs.Files{i};
    [~, name, ext] = fileparts(img2Name);
    disp(['processing ' name]);
    outputPath = strcat(outputDir, '\', name, ext);
    
    % read images 
    img2 = imread(img2Name);
    img2pP = simplePreprocess (img2, 0, 30, 0, 0); %  (in, clahesize, highpsize, intenscap, winSize)
    img2 = img2pP;
    % calculate shift in gcps
    oldGCPs = zeros(size(gcps));
    newGCPs = zeros(size(gcps));
    canBeTransformed = 1;
    for j = 1:size(gcps, 1)
        points = calcPointPairs(img1, img2, gcps(j, :), IA);
        if isnan(points)
            disp(['Cannot calculate shift, img ' name ' will be skipped']);
            canBeTransformed = 0;
            break;
        end
        oldGCPs(j, 1) = points(1);
        oldGCPs(j, 2) = points(2);
        newGCPs(j, 1) = points(3);
        newGCPs(j, 2) = points(4);
    end % for j
    if canBeTransformed
        transform(img1, img2, outputPath, oldGCPs, newGCPs, transType);
        disp([name ' transformed']);
    end
end 

outputPath = strcat(outputDir, '\', file);
imwrite(img1, outputPath);
disp('Done');

end 


function points = calcPointPairs(img1, img2, curr_gcps, IA)
    points = [];
    x = curr_gcps(1);
    y = curr_gcps(2);
    buff = IA / 2 * 3;
    roi_x = x - buff;
    roi_y = y - buff;
    if roi_x < 0 || roi_y < 0
        return;
    end
    area = buff * 2;
    roirect = [roi_x, roi_y, area, area];
    refine = IA / 2;
    [x, y, u, v, ~] = piv_FFTmulti (img1, img2, IA, refine, ...
        1, [], roirect, 2, refine, refine, refine, '*linear', 0, 0);
    iy = ceil(size(x, 1) / 2);
    ix = ceil(size(x, 2) / 2);
    points(1) = x(iy, ix);
    points(2) = y(iy, ix);
    points(3) = x(iy, ix) + u(iy, ix);
    points(4) = y(iy, ix) + v(iy, ix);
end
