%% Correct Image for Lens Distortion
%
%% 
% Create a set of calibration images.
disp('Create a set of calibration images.');
base_dir = 'C:\Projects\Fishstream\Calibration\';
inputDir = uigetdir(base_dir, 'Select an input folder');
outputDir = uigetdir(base_dir, 'Select an output folder');
images = imageDatastore(inputDir);
%images = imageDatastore(fullfile(toolboxdir('vision'),'visiondata', ...
%    'calibration','mono'));
%%
% Detect calibration pattern.
disp('Detect calibration pattern.');
[imagePoints,boardSize] = detectCheckerboardPoints(images.Files);
%%
% Generate world coordinates of the corners of the squares. The square 
% size is in millimeters.
disp('Generate world coordinates of the corners of the squares.');
squareSize = 72;
worldPoints = generateCheckerboardPoints(boardSize,squareSize);
%%
% Calibrate the camera.
disp('Calibrate the camera.');
I = readimage(images,1); 
imageSize = [size(I,1),size(I,2)];
params = estimateFisheyeParameters(imagePoints,worldPoints,imageSize);
save('fishEyeParams_20190913', 'params');
cameraParams = estimateCameraParameters(imagePoints,worldPoints, ...
                                  'ImageSize',imageSize);
save('cameraParams_20190913', 'cameraParams');
%%
% Remove lens distortion and display results.
disp('Remove lens distortion and display results.');
% I = images.readimage(1);
[file,path] = uigetfile(strcat(inputDir, '\*.jpg'), 'Select an image to remove distortion');
I = imread(strcat(path, file));
J1 = undistortImage(I,cameraParams);
J2 = undistortFisheyeImage(I,params.Intrinsics);
%%
%figure; imshowpair(I,J2,'montage');
%title('Original Image (left) vs. Corrected Image (right)');
%%
%J2 = undistortImage(I,cameraParams,'OutputView','full');
%figure; 
%imshow(J2);
%title('Full Output View');
outputPathSelect = strcat(outputDir, '\', file);
imwrite(J2, outputPathSelect);
disp('Done');