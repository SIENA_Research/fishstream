function stabilise_shaky (gcps, IA, step, modelNr, transType, preprocess)

base_dir = 'C:\Projects\Fishstream\';
inputDir = uigetdir(base_dir, 'Select an input folder');
outputDir = uigetdir(base_dir, 'Select a folder for stabilized output');
%[file,path] = uigetfile(strcat(base_dir, '\*.jpg'), 'Select a model image'); % or '\*.tif'
summativeDiffFile = strcat(outputDir, '\', 'summativeDiff.txt');
frameByFrameDiffFile = strcat(outputDir, '\', 'frameByFrameDiff.txt');
summativeDiff = fopen(summativeDiffFile, 'a+');
frameByFrameDiff = fopen(frameByFrameDiffFile, 'a+');

% basento1 = [291, 272; 318, 598; 301, 802; 1520, 299; 1458, 477; 1526, 890];
% basento2 = [292, 160; 304, 472; 315, 919; 320, 798; 1831, 788; 1454, 779; 1467, 675; 1778, 162; 1470, 185];
if isnan(gcps)
    gcps = [233, 431; 1085, 769; 1802, 117; 3546, 353; 2889, 438];
end
if isnan(transType)
    transType = 'affine';  % 'projective'; % 
end
% create a datastore for all images to transform
imgs = imageDatastore(inputDir, 'FileExtensions',{'.jpg','.png'});
% read the model image
%img1 = imread(strcat(path, file));
oldGCPs = gcps; % gcps in img i
format = '%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f\n';
summativeDifferences = zeros(size(gcps));
frameByFrameDifferences = zeros(size(gcps));

% write model img to output
outputPath = strcat(outputDir, '\', '_model.jpg');
img1 = imread(imgs.Files{modelNr});
imwrite(img1, outputPath);

for i = modelNr:step:size(imgs.Files, 1)-1
    img1Name = imgs.Files{i};
    img2Name = imgs.Files{i+1};
    [~, name, ext] = fileparts(img2Name);
    disp(['processing ' name]);
    outputPath = strcat(outputDir, '\', name, ext);
    pPname = '';
    if i < 10
    	pPname = '000';
    else
        if i < 100
        pPname = '00';
        else
            if i < 1000
                pPname = '0';
            end
        end
    end
    outputPp = strcat(outputDir, '\pp\', pPname, num2str(i), ext);
    % read images 
    img1 = imread(img1Name);
    img2 = imread(img2Name);
    if (preprocess)
        img1pP = simplePreprocess (img1, 0, 30, 0, 0);
        img2pP = simplePreprocess (img2, 0, 30, 0, 0); %  (in, clahesize, highpsize, intenscap, winSize)
    end
    % calculate shift in gcps
    newGCPs = zeros(size(gcps)); % gcps in img i+1
    canBeTransformed = 1;
    for j = 1:size(oldGCPs, 1)
        if preprocess > 0
            points = calcPointPairs(img1pP, img2pP, oldGCPs(j, :), IA);
        else
            points = calcPointPairs(img1, img2, oldGCPs(j, :), IA);
        end
        if isnan(points)
            disp(['Cannot calculate shift, img ' name ' will be skipped']);
            canBeTransformed = 0;
            break;
        end 
        newGCPs(j, 1) =  oldGCPs(j, 1) + points(3);
        newGCPs(j, 2) =  oldGCPs(j, 2) + points(4);
        summativeDifferences(j, 1) = summativeDifferences(j, 1) + points(3);
        summativeDifferences(j, 2) = summativeDifferences(j, 2) + points(4);
        frameByFrameDifferences(j, 1) = points(3);
        frameByFrameDifferences(j, 2) = points(4);
    end % for j
    fprintf(summativeDiff, format, summativeDifferences);
    fprintf(frameByFrameDiff, format, frameByFrameDifferences);
    if canBeTransformed
        transform(img1, img2, outputPath, gcps, newGCPs, transType);
        if preprocess == 1
            transform(img1, img2, outputPp, gcps, newGCPs, transType);
        end
        if preprocess == 2
            transform(img1pP, img2pP, outputPp, gcps, newGCPs, transType);
        end
        oldGCPs = newGCPs; % in the next iteration, current img i+1 will be img i
        disp([name ' transformed']);
    end
end 


disp('Done');
fclose(summativeDiff);
fclose(frameByFrameDiff);
end 


function points = calcPointPairs(img1, img2, curr_gcps, IA)
    points = [];
    x = curr_gcps(1);
    y = curr_gcps(2);
    buff = IA / 2 * 3;
    roi_x = x - buff;
    roi_y = y - buff;
    if roi_x < 0 || roi_y < 0
        return;
    end
    area = buff * 2;
    roirect = [roi_x, roi_y, area, area];
    refine = IA / 2;
    [x, y, u, v, ~] = piv_FFTmulti (img1, img2, IA, refine, ...
        1, [], roirect, 2, refine, refine, refine, '*linear', 0, 0);
    iy = ceil(size(x, 1) / 2);
    ix = ceil(size(x, 2) / 2);
    points(1) = x(iy, ix);
    points(2) = y(iy, ix);
    points(3) = u(iy, ix);
    points(4) = v(iy, ix);
end
