% transform() translates the second image (img2) in order to align it with
% the first image (img1) based on control points (currently - 3 for affine
% transformation) present in both images. Coordinates of these points in
% the model image (img1) are passed as a 3 x 2 array gcps; coordinates of
% the same points in the image that has to be transformed (img2) are passed
% in an array newGcps. @param outputName is the name under which the
% transformed image has to be stored

function transform(img1, img2, outputName, gcps, newGcps, transformType)

tform = fitgeotrans(newGcps, gcps, transformType);
viewSameSize = imref2d(size(img1));

%disp('Warping')
transformedImage = imwarp(img2, tform, 'OutputView', viewSameSize);

%disp('Writing')
imwrite(transformedImage, outputName);
%disp(strcat(outputName, ' transformed'))


























% old version of transform

% function transform(input_dir, output_dir, img1Name, img2Name, gcps, newGcps)
% % dir = 'E:\projects\FishStream_Data\Matlab_tests\extract_test\test_pystab\';
% % disp('Reading images')
% % img1 = imread(strcat(dir, '13.jpg'));
% % img2 = imread(strcat(dir, '14.jpg'));
% % disp('Calculating the transformation object')
% % img1Points = [3133 1332; 3434 1195; 3417 1388];
% % %img2Points = [3133.7 1332.1; 3434.77 1195.09; 3417.78 1388.12];
% % img2Points = [3.1336571e+03 1.3321161e+03;3.4346507e+03 1.1951123e+03;3.4176941e+03 1.3880996e+03];
% % tform = fitgeotrans(img2Points, img1Points, 'affine');
% % viewSameSize = imref2d(size(img1));
% % disp('Warping')
% % opt = imwarp(img2, tform, 'OutputView', viewSameSize);
% % disp('Writing')
% % imwrite(opt, strcat(dir, '12.jpg'));
% % disp('Done')
% 
% disp('Reading images')
% img1 = imread(strcat(input_dir, img1Name));
% img2 = imread(strcat(input_dir, img2Name));
% 
% disp('Calculating the transformation object')
% tform = fitgeotrans(newGcps, gcps, 'affine');
% viewSameSize = imref2d(size(img1));
% 
% disp('Warping')
% transformedImage = imwarp(img2, tform, 'OutputView', viewSameSize);
% 
% disp('Writing')
% imwrite(transformedImage, strcat(output_dir, img2Name));
% disp(strcat(img2Name, ' transformed'))