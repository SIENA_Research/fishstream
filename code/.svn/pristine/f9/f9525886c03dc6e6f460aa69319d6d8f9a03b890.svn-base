function compareWithReference(inputDir, transformType, degree, distance)
    %% compare reference measurements with calculated values
    % select a file with coordinate pairs real/screen in the format
    % x_screen, y_screen, x_real, y_real
    [pairsName, pairsPath] = uigetfile(strcat(inputDir, '\*.txt'), ...
                            'Select coordinate pairs (screen/reference)');
    formatSpec = '%f %f %f %f';
    colNum = 4;
    pairsFile = fopen(fullfile(pairsPath, pairsName), 'r');
    sizeOfArray = [colNum inf];
    pairs = fscanf(pairsFile, formatSpec, sizeOfArray);
    fclose(pairsFile);
    
    pairs = pairs.';
    screen = pairs(:, 1:2);
    real = pairs(:, 3:4);
    clear pairs;
    
    % create a transformation object
    if strcmp(transformType, 'polynomial')
        tform = fitgeotrans(screen, real, transformType, degree); % 'polynomial', 2
    else
        tform = fitgeotrans(screen, real, transformType); % 'affine'
    end
    
    clear screen;
    clear real;
    
    % load reference measurements in format id, x_real, y_real, velocity, angle
    [refName, refPath] = uigetfile(strcat(inputDir, '\*.txt'), 'Select reference measurements');
    formatSpec = '%d %f %f %f %f';
    colNum = 5;
    refFile = fopen(fullfile(refPath, refName), 'r');
    sizeOfArray = [colNum inf];
    refs = fscanf(refFile, formatSpec, sizeOfArray);
    fclose(refFile);
    refs = refs.';
    
    %extract coordinates from references
    refCoords = refs(:, 2:3);
    transformedRefCoords = transformPointsInverse(tform, refCoords);
    
    % load calibrated vectors from pivlab
    % x [m]	y [m]	u [m/s]	v [m/s]	vorticity [1/s]	magnitude [m/s]	divergence [1/s]	dcev [1]	simple shear [1/s]	simple strain [1/s]	vector direction [degrees]
    
    [vecName, vecPath] = uigetfile(strcat(inputDir, '\*.txt'), 'Select calculated vectors');
    formatSpec = '%f %f %f %f %f %f %f %f %f %f %f';
    colNum = 11;
    vecFile = fopen(fullfile(vecPath, vecName), 'r');
    sizeOfArray = [colNum inf];
    vecs = fscanf(vecFile, formatSpec, sizeOfArray);
    fclose(vecFile);
    vecs = vecs([1, 2, 6, 11], :);
    xs = vecs(1, :);
    ys = vecs(2, :);
    clear colNum;
    clear sizeOfArray;
    clear formatSpec;
    
    % create a results array i a format 
    % ref_id, ref_x, ref_y, ref_val, x, y, piv_min, piv_max, piv_mean
    numOfRefs = size(refs, 1);
    res = [refs(:, 1), transformedRefCoords, refs(:, 2:4), zeros(numOfRefs, 3)]; 
    for i = 1:numOfRefs
        % define the search region as min and max coordinates
        upperLimX = transformedRefCoords(i, 1) + distance;
        lowerLimX = transformedRefCoords(i, 1) - distance;
        upperLimY = transformedRefCoords(i, 2) + distance;
        lowerLimY = transformedRefCoords(i, 2) - distance;
        % find indices of suitable coordinates in vector array
        insX = xs >= lowerLimX & xs <= upperLimX;
        insY = ys >= lowerLimY & ys <= upperLimY;
        ins = insX .* insY;
        [~, col] = find (ins > 0);
        % get velocity values from the surrounding
        surrounding = vecs(:, col);
        velocities = surrounding(3, :);
        disp(velocities);
        if isempty(velocities)
            res(i, 7) = -1;
            res(i, 8) = -1;
            res(i, 9) = -1;
        else
            % calculate min, max, mean and store in results
            res(i, 7) = min(velocities);
            res(i, 8) = max(velocities);
            res(i, 9) = mean(velocities);
        end
    end
    % find min, max, mean within distance (in m) from reference
    % measurements

    disp(res);
    
    %%