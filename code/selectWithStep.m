% select each n-th file from the folder
inputDir = uigetdir('E:\projects\FishStream_Data\20190807_Rottau\mavic\', 'Select an input folder');
outputDir = uigetdir(inputDir, 'Select a folder for selection');

imgs = imageDatastore(inputDir, 'FileExtensions',{'.jpg','.png'});
firstIndex = 1;
step = 200;

for i = firstIndex:step:size(imgs.Files, 1)
    img2Name = imgs.Files{i};
    [~, name, ext] = fileparts(img2Name);
    disp(['copying ' name]);
    outputPath = strcat(outputDir, '\', name, ext);
    
    % read images 
    img2 = imread(img2Name);
    imwrite(img2, outputPath);
end

disp('Tadaaam!');
