
% inputDir where references and vectors are stored, distance in px, scale
% in px/m
function res = compareWithReference(inputDir, distance, scale, outputName)
% load references in screan coordinates
[refName, refPath] = uigetfile(strcat(inputDir, '\*.txt'),...
    'Select references in screen coordinates');
formatSpec = '%d %f %f %f %f'; % id x y velocity angle_CW_N
colNum = 5; 
refFile = fopen(fullfile(refPath, refName), 'r');
sizeOfArray = [colNum inf];
refs = fscanf(refFile, formatSpec, sizeOfArray);
fclose(refFile);
refs = refs.';

refCoords = refs(:, 2:3);

% load calibrated vectors
[vecName, vecPath] = uigetfile(strcat(inputDir, '\*.txt'), 'Select a file with vectors');
formatSpec = '%f %f %f %f %f %f %f %f %f %f %f';
colNum = 11;
vecFile = fopen(fullfile(vecPath, vecName), 'r');
sizeOfArray = [colNum inf];
vecs = fscanf(vecFile, formatSpec, sizeOfArray);
fclose(vecFile);
vecs = vecs([1, 2, 6, 11], :);
xs = round(vecs(1, :) / scale);
ys = round(vecs(2, :)/ scale);

clear colNum;
clear sizeOfArray;
clear formatSpec;

% select vectors within distance from references
numOfRefs = size(refs, 1);
res = [refs(:, 1), refCoords, refs(:,4), zeros(numOfRefs, 4)];

% calculate min, max, mean, median of the vectors
for i = 1:numOfRefs
    % limit the search region to the area between min and max coordinates
    % of references +/- buffer distance
    x = refCoords(i, 1);
    y = refCoords(i, 2);
    upX = x + distance;
    lowX = x - distance;
    upY = y + distance;
    lowY = y - distance;
    
    % find indexes of vectors between the limits
    insX = xs >= lowX & xs <= upX;
    insY = ys >= lowY & ys <= upY;
    ins = insX .* insY;
    [~, col] = find(ins > 0);
    % get velocity values from neighbor vectors
    neighbors = vecs(:, col);
    velocities = neighbors(3, :);
    if isempty(velocities)
        res(i, 5) = -1;
        res(i, 6) = -1;
        res(i, 7) = -1;
        res(i, 8) = -1;
    else
        % calculate min, max, mean and store in results
        res(i, 5) = min(velocities);
        res(i, 6) = max(velocities);
        res(i, 7) = mean(velocities);
        res(i, 8) = median(velocities);
    end
end
% save as tab separated text: id, ref, min, max, mean, median

dlmwrite(strcat(inputDir, '\', outputName), res, '\t');
