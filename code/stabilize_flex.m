function stabilize_flex (gcps, IA, step, modelNr, transType)

base_dir = 'C:\Projects\Fishstream\Stabilisation\Basento\';
inputDir = uigetdir(base_dir, 'Select an input folder');
outputDir = uigetdir(base_dir, 'Select a folder for stabilized output');
%[file,path] = uigetfile(strcat(base_dir, '\*.jpg'), 'Select a model image'); % or '\*.tif'
summativeDiffFile = strcat(outputDir, '\', 'summativeDiff.txt');
frameByFrameDiffFile = strcat(outputDir, '\', 'frameByFrameDiff.txt');
summativeDiff = fopen(summativeDiffFile, 'a+');
frameByFrameDiff = fopen(frameByFrameDiffFile, 'a+');

basento = [298, 155; 310, 469; 316, 574; 302, 801; 294, 919;...
            1441, 210; 1471, 494; 1463, 671; 1440, 863];

basento14 = [289, 154; 312, 468; 291, 272; 318, 598; 301, 802; 340, 872; 1427, 136;...
           1520, 299; 1458, 477; 1526, 890; 1565, 631; 1484, 868; 1761, 379; 1732, 790];
basento2 = [291, 272; 318, 598; 301, 802; ...
           1520, 299; 1458, 477; 1526, 890];
gcps = [292, 160; 304, 472; 315, 919; 320, 798;...
           1831, 788; 1454, 779; 1467, 675; 1778, 162; 1470, 185];

% create a datastore for all images to transform
imgs = imageDatastore(inputDir, 'FileExtensions',{'.jpg','.png'});
% read the model image
%img1 = imread(strcat(path, file));
if isnan(gcps)
    gcps = basento;
end
oldGCPs = gcps; % gcps in img i
format = '%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f\n';
format14 = '%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f	%.5f\n';
summativeDifferences = zeros(size(gcps));
frameByFrameDifferences = zeros(size(gcps));

% write model img to output
outputPath = strcat(outputDir, '\', '_model.jpg');
img1 = imread(imgs.Files{modelNr});
imwrite(img1, outputPath);

for i = modelNr:step:size(imgs.Files, 1)-1
    img1Name = imgs.Files{i};
    img2Name = imgs.Files{i+1};
    [~, name, ext] = fileparts(img2Name);
    disp(['processing ' name]);
    outputPath = strcat(outputDir, '\', name, ext);
    
    % read images 
    img1 = imread(img1Name);
    img2 = imread(img2Name);
    % img2pP = simplePreprocess (img2, 0, 5, 1, 100, 0, 1, 20);
    % calculate shift in gcps
    newGCPs = zeros(size(gcps)); % gcps in img i+1
    canBeTransformed = 1;
    for j = 1:size(oldGCPs, 1)
        points = calcPointPairs(img1, img2, oldGCPs(j, :), IA);
        newGCPs(j, 1) =  oldGCPs(j, 1) + points(3);
        newGCPs(j, 2) =  oldGCPs(j, 2) + points(4);
        summativeDifferences(j, 1) = summativeDifferences(j, 1) + points(3);
        summativeDifferences(j, 2) = summativeDifferences(j, 2) + points(4);
        frameByFrameDifferences(j, 1) = points(3);
        frameByFrameDifferences(j, 2) = points(4);
        if isnan(points)
            disp(['Cannot calculate shift, img ' name ' will be skipped']);
            canBeTransformed = 0;
            break;
        end 
    end % for j
    fprintf(summativeDiff, format, summativeDifferences);
    fprintf(frameByFrameDiff, format, frameByFrameDifferences);
    if canBeTransformed
        transform(img1, img2, outputPath, gcps, newGCPs, transType);
        oldGCPs = newGCPs; % in the next iteration, current img i+1 will be img i
        disp([name ' transformed']);
    end
end 


disp('Done');
fclose(summativeDiff);
fclose(frameByFrameDiff);
end 


function points = calcPointPairs(img1, img2, curr_gcps, IA)
    points = [];
    x = curr_gcps(1);
    y = curr_gcps(2);
    buff = IA * 1.5;
    roi_x = x - buff/2;
    roi_y = y - buff/2;
    if roi_x < 0 || roi_y < 0
        return;
    end
    roirect = [roi_x, roi_y, buff, buff];
    refine = IA / 2;
    [x, y, u, v, ~] = piv_FFTmulti (img1, img2, IA, refine, ...
        1, [], roirect, 2, refine, refine, refine, '*linear', 0, 0);
    iy = ceil(size(x, 1) / 2);
    ix = ceil(size(x, 2) / 2);
    points(1) = x(iy, ix);
    points(2) = y(iy, ix);
    points(3) = u(iy, ix);
    points(4) = v(iy, ix);
end


function stabilize_flex3()

base_dir = 'C:\Projects\Fishstream\Stabilisation\Basento\';
dir = uigetdir(base_dir, 'Select an input folder');
output_dir = uigetdir(base_dir, 'Select a folder for stabilized output');
[file, path] = uigetfile(strcat(dir, '\*.jpg'), 'Select a reference image'); % or '\*.tif'
img1 = imread(strcat(path, file));
imgs = imageDatastore(dir, 'FileExtensions',{'.jpg','.png'});

%imshow([img1; img2]);

% estimate transformation
for i = 1: size(imgs.Files, 1)
    img2_name = imgs.Files{i};
    [~, name, ext] = fileparts(img2_name);
    disp(['processing ' name]);
    output_path = strcat(output_dir, '\', name, ext);
    % read images 
    img2 = imread(img2_name);
    % estimate distortion
    tform_estimate = imregcorr(img2, img1);

    % transform using the estimation
    s = imref2d(size(img1));
    img2 = imwarp(img2, tform_estimate, 'OutputView', s);
    imwrite(img2, output_path);
end
end


function stabilize_flex2()

base_dir = 'C:\Projects\Fishstream\Stabilisation\Basento\';
dir = uigetdir(base_dir, 'Select an input folder');
% output_dir = uigetdir(base_dir, 'Select a folder for stabilized output');
[file, path] = uigetfile(strcat(dir, '\*.jpg'), 'Select a reference image'); % or '\*.tif'
img1 = imread(strcat(path, file));
[file, path] = uigetfile(strcat(dir, '\*.jpg'), 'Select an image to transform'); % or '\*.tif'
img2 = imread(strcat(path, file));

%imshow([img1; img2]);

% estimate transformation
tform_estimate = imregcorr(img2, img1);

% transform using the estimation
s = imref2d(size(img1));
img2_reg = imwarp(img2, tform_estimate, 'OutputView', s);

imshow([img2; img2_reg]);
imshowpair(img1, img2_reg, 'falsecolor');

% finish the registration
[optimizer, metric] = imregconfig('monomodal');
img2_tr = imregister(rgb2gray(img2), rgb2gray(img1),...
    'affine', optimizer, metric, 'InitialTransformation', tform_estimate);

imshowpair(img1, img2_tr, 'falsecolor');

end