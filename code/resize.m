function resize (factor)

base_dir = 'C:\Projects\Fishstream\Stabilisation\';
inputDir = uigetdir(base_dir, 'Select an input folder');
outputDir = uigetdir(base_dir, 'Select a folder for the scaled output');
imgs = imageDatastore(inputDir, 'FileExtensions',{'.jpg','.png'});

for i=1:size(imgs.Files, 1)
    img = imread(imgs.Files{i});
    bigImg = imresize(img, factor);
    [~, name, ext] = fileparts(imgs.Files{i});
    outName = strcat(outputDir, '\', name, ext);
    imwrite(bigImg, outName);
end

disp('Done resizing');

end


