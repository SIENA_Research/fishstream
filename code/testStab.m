saveSelection = 0; % 0 = no, 1 = yes
% base directory for data selection and storage
base_dir = 'C:\Projects\Fishstream\Stabilisation\';

inputDir = uigetdir(base_dir, 'Select an input folder');
if saveSelection
    outputDirSelection = uigetdir(inputDir, 'Select a folder for selection');
end
outputDir = uigetdir(base_dir, 'Select a folder for stabilized output');

[file,path] = uigetfile(strcat(base_dir, '\*.jpg'), 'Select a model image'); % or '\*.tif'

%buffer aroung GCPs for displacement calculation
distanceFromGCP = 64;
step = 1; % each nth image will be processed
firstNonModel = 2;
% min 4 points for projective, min 3 points for affine
method = 1; % 2 = 'projective'; 1 = 'affine'
% 'FileExtensions',{'.tif'}
imgs = imageDatastore(inputDir, 'FileExtensions',{'.jpg','.png'});
img1 = imread(strcat(path, file));
img1pP = simplePreprocess (img1, 0, 5, 1, 100, 0, 1, 20);

% define gcp coordinates                       
gcps = [399, 304; 3206, 232; 532, 2001; 3238, 1721];
% define extra motionless features in case one of the GCPs does not get processed correctly
extraPoints = [1672, 308; 2427, 319; 1651, 1813; 2603, 1820];
%--------------------------------------------------------
%%
for i = firstNonModel:step:size(imgs.Files, 1)
    img2Name = imgs.Files{i};
    [~, name, ext] = fileparts(img2Name);
    disp(['processing ' name]);
    outputPath = strcat(outputDir, '\', name, ext);
    
    % read images 
    img2 = imread(img2Name);
    img2pP = simplePreprocess (img2, 0, 5, 1, 100, 0, 1, 20);
    %write selection
    if saveSelection
        outputPathSelect = strcat(outputDirSelection, '\', name, ext);
        imwrite(img2, outputPathSelect);
    end
    % calculate shift in gcps
    points = shiftCalc(img1pP, img2pP, gcps, extraPoints, IA);
    
    if isnan(points)
        disp(['Cannot calculate shift, img ' name ' will be skipped']);
    else
        gcps = points(:, 1:2);
        newGcps = points(:, 3:4);
        transform(img1, img2, outputPath, gcps, newGcps, method);
        disp([name ' transformed']);
    end
end    

%  write the model image to the output directory
outputPath = strcat(outputDir, '\', file);
imwrite(img1, outputPath);
if saveSelection
    outputPathSelect = strcat(outputDirSelection, '\', file);
    imwrite(img1, outputPathSelect);
end

disp('Done');