function out = simplePreprocess (in, clahesize, highpsize, intenscap, winSize)

if size(in,3)>1
    in(:,:,2:end)=[];
end

%histogramm anpassen, bei 8bit und 16 bit
B = imadjust(in, stretchlim(in),[]);
B=(double(B)/max(max(double(B))));

in=uint8(B*255); %convert back to uint8

if intenscap == 1
    %Intensity Capping: a simple method to improve cross-correlation PIV results
    %Uri Shavit � Ryan J. Lowe � Jonah V. Steinbuck
    n = 2; 
    up_lim_im_1 = median(double(in(:))) + n*std2(in); % upper limit for image 1
    brightspots_im_1 = in > up_lim_im_1; % bright spots in image 1
    capped_im_1 = in; 
    capped_im_1(brightspots_im_1) = up_lim_im_1; % capped image 1
    in=capped_im_1;
end
if clahesize > 0
    numberoftiles1=round(size(in,1)/clahesize);
    numberoftiles2=round(size(in,2)/clahesize);
    if numberoftiles1 < 2
    numberoftiles1=2;
    end
    if numberoftiles2 < 2
    numberoftiles2=2;
    end
    in=adapthisteq(in, 'NumTiles',[numberoftiles1 numberoftiles2],...
        'ClipLimit', 0.01, 'NBins', 256, 'Range', 'full',...
        'Distribution', 'uniform');
end

if highpsize > 0
    h = fspecial('gaussian',highpsize,highpsize);
    in=double(in-(imfilter(in,h,'replicate')));
    in=in/max(max(in))*255;
end

if winSize > 0
    in=wiener2(in,[winSize winSize]);
end

out=uint8(in);
