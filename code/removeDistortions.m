function removeDistortions(base_dir, var_path)

%% load images
inputDir = uigetdir(base_dir, 'Select an input folder');
outputDir = uigetdir(base_dir, 'Select an output folder');
images = imageDatastore(inputDir);

%% load camera params
storedCameraParams = load(var_path);
cameraParams = storedCameraParams.cameraParams;


for i = 1:size(images.Files, 1)
    curr = images.Files{i};
    [~, fname, fext] = fileparts(curr);
    image = imread(curr);
    [image2, ] = undistortImage(image, cameraParams);
    outNameSpec = '%s/%s_u%s';
    outName = sprintf(outNameSpec, outputDir, fname, fext);
    imwrite(image2, outName);
end
disp('done');
