% calculate image transformation based on coordinate pairs
% screen/real world 
inputDir = 'E:\';
% select a file with coordinate pairs in the format
    % x_screen, y_screen, x_real, y_real
[pairsName, pairsPath] = uigetfile(strcat(inputDir, '\*.txt'), ...
                        'Select coordinate pairs (screen/reference)');
formatSpec = '%f %f %f %f';
colNum = 4;
pairsFile = fopen(fullfile(pairsPath, pairsName), 'r');
sizeOfArray = [colNum inf];
pairs = fscanf(pairsFile, formatSpec, sizeOfArray);
fclose(pairsFile);

pairs = pairs.';
screen = pairs(:, 1:2);
real = pairs(:, 3:4);
clear pairs;

% create a transformation object
tform = fitgeotrans(screen, real, 'affine'); % 

clear screen;
clear real;

% calculate rotation angle of the transformed image

tformInv = invert(tform);
Tinv = tformInv.T;
ss = Tinv(2,1);
sc = Tinv(1,1);
theta_recovered = atan2(ss,sc)*180/pi;

% get vector data from a file



g = hgtransform;
quiver(x,y,u,v,'Parent',g);
set(g,'Matrix',makehgtform('zrotate',theta_recovered));
figure
quiver(x,y,u,v)